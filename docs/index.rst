===================
Data Request Broker
===================
---------------------------------
S3 driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-s3/month
    :target: https://pepy.tech/project/drb-driver-s3
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-s3.svg
    :target: https://pypi.org/project/drb-driver-s3/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-s3.svg
    :target: https://pypi.org/project/drb-driver-s3/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-s3.svg
    :target: https://pypi.org/project/drb-driver-s3/
    :alt: Python Version Support Badge

-------------------


User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

Others
======
.. toctree::
   :maxdepth: 2

   user/limitation
