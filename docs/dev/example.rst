.. _example:

Example
=======

Connect to your server
----------------------
.. literalinclude:: example/connect.py
    :language: python

Download a file
----------------

.. literalinclude:: example/download.py
    :language: python

List container and Object
---------------------------
.. literalinclude:: example/children.py
    :language: python