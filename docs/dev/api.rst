.. _api:

Reference API
=============

This driver wraps amazon simple storage service (s3) in drb node.
It provides a set of nodes representing the service it self (DrbS3Service),
the bucket representation (DrbS3Bucket),
and the object representation (DrbS3Object).
All these representations follow DrbNode data model and their access
methods are optimized to speedup accesses to openstack service.


Auth
----------
.. autoclass:: drb.drivers.aws3.Auth
    :members:

DrbS3Service
------------
.. autoclass:: drb.drivers.aws3.DrbS3Service
    :members:

DrbS3Bucket
------------
.. autoclass:: drb.drivers.aws3.DrbS3Bucket
    :members:

DrbS3Object
------------
.. autoclass:: drb.drivers.aws3.DrbS3Object
    :members: