from botocore.config import Config
from drb.drivers.aws3 import DrbS3Service, Auth

auth = Auth(service_name='s3',
            endpoint_url='http://your_s3_storage/',
            aws_access_key_id='user',
            aws_secret_access_key='password',
            config=Config(signature_version='s3v4'),
            region_name='us-east-1')
node = DrbS3Service(auth)
