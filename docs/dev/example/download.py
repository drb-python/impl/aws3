import io
from botocore.config import Config
from drb.drivers.aws3 import Auth, DrbS3Service

auth = Auth(service_name='s3',
            endpoint_url='http://your_s3_storage/',
            aws_access_key_id='user',
            aws_secret_access_key='password',
            config=Config(signature_version='s3v4'),
            region_name='us-east-1')
node = DrbS3Service(auth)

obj = node['bucket']['object.png']

# Read all the file
with obj.get_impl(io.BytesIO) as stream:
    stream.read()

with open("object.png", 'w') as file:
    # Download file by using a temp url
    with obj.get_impl(io.BytesIO, temp_url=True) as stream:
        file.write(stream.read())

