from botocore.config import Config
from drb.drivers.aws3 import Auth, DrbS3Service

auth = Auth(service_name='s3',
            endpoint_url='http://your_s3_storage/',
            aws_access_key_id='user',
            aws_secret_access_key='password',
            config=Config(signature_version='s3v4'),
            region_name='us-east-1'
            )

node = DrbS3Service(auth)

# List all the buckets
for e in node:
    # Do Something with your children
    print(e.name)

child = node['bucket-1']

# List all object
for e in child:
    # Do Something with your children
    print(e.name)