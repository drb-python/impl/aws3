.. _install:

Installation of s3 driver
====================================
Installing ``drb-driver-aws3`` with execute the following in a terminal:

.. code-block::

   pip install drb-driver-aws3
