.. _limitation:

Limitations
-----------
This driver doesn't allow to write, modify, delete file on a s3 bucket,
or it doesn't allow to delete or upload a file.
This driver doesn't allow to download directly a bucket.