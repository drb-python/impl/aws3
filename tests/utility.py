from io import StringIO

import httpretty


class S3Obj:
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
        self.key = name

    def get_available_subresources(self):
        if self.name.startswith('my_bucket') or self.name == 'tests':
            return ['Content_size', 'type']
        pass

    def Content_size(self):
        return 123

    def type(self):
        return str


class DownloadObject:
    def __init__(self, data):
        self.buff = data

    @property
    def content_length(self):
        return len(self.buff)

    def get(self, Range=range):
        return {"Body": StringIO(self.buff)}


def start_mock(svc_url: str):
    def download(request, uri, headers):
        data = bytes("This is for testing.", 'utf-8')
        return 200, headers, data

    httpretty.enable()
    httpretty.register_uri(httpretty.GET, f"{svc_url}",
                           download)


def stop_mock():
    httpretty.disable()
    httpretty.reset()
